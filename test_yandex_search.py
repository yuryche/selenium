from selenium.webdriver.chrome.webdriver import WebDriver
from selenium.webdriver.support.wait import WebDriverWait
import time
import allure

@allure.title('results are more 12')
def test_yandex_search():
    driver = WebDriver(executable_path='C://Workspace//chromedriver.exe')
    driver.get('https://ya.ru')
    search_input = driver.find_element_by_xpath('//input[@id="text"]')
    search_button = driver.find_element_by_xpath('//button[@type="submit"]')
    search_input.send_keys("market.yandex.ru")
    search_button.click()

    def check_results_count(driver):
        inner_search_results = driver.find_elements_by_xpath('//*[@class="organic__url-text"]')
        return len(inner_search_results) >= 12

    WebDriverWait(driver, 5, 0.5).until(check_results_count) #run this wait during 5 sec every 0.5 sec
    search_results = driver.find_elements_by_xpath('//li[@class="serp-item"]')
    link = search_results[1].find_element_by_xpath('.//h2/a')
    print("current link is", link)
    link.click()

    driver.switch_to.window(driver.window_handles[1])
    time.sleep(2)

    assert driver.title == "Яндекс.Маркет — выбор и покупка товаров из проверенных интернет-магазинов"
