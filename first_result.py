from selenium.webdriver.chrome.webdriver import WebDriver
from selenium.webdriver.support.wait import WebDriverWait
import time
import unittest
from selenium.webdriver.common.keys import Keys

class Google_test(unittest.TestCase):
    def setUp(self):
        self.driver = WebDriver(executable_path='C://Workspace//chromedriver.exe')
        self.driver.get("https://yandex.ru")
        time.sleep(5)

    def test_01(self):
        driver = self.driver
        input_field = driver.find_element_by_xpath('//input[@id="text"]')
        input_field.send_keys("Python")
        input_field.send_keys(Keys.ENTER)

        def check_results_count(driver):
            inner_search_results = driver.find_elements_by_xpath('//*[@class="organic__url-text"]')
            return len(inner_search_results) >= 10

        WebDriverWait(driver, 5, 0.5).until(check_results_count)
        titles = driver.find_elements_by_xpath('//div[@class="organic__url-text"]')
        for title in titles:
            assert "python" in title.text.lower()
            print(title.text.lower()) #Printing titles found
        print("Test finished OK")

    def tearDown(self):
        self.driver.quit()

if __name__=='__main__':
    unittest.main()
