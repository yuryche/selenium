from selenium.webdriver.chrome.webdriver import WebDriver
from selenium.webdriver.support.wait import WebDriverWait
import time
import unittest
from selenium.webdriver.common.keys import Keys

class Butylka(unittest.TestCase):
    def setUp(self):
        self.driver = WebDriver(executable_path='C://Workspace//chromedriver.exe')
        self.driver.get("https://yandex.ru")

    def test_01(self):
        requests = ["пластиковая бутылка в нижнем новгороде"]
        driver = self.driver
        for i in requests:
            input_field = driver.find_element_by_xpath('//input[@id="text"]')
            input_field.send_keys(i)
            input_field.send_keys(Keys.ENTER)
            def check_results_count(driver):
                inner_search_results = driver.find_elements_by_xpath('//*[@class="organic__url-text"]')
                return len(inner_search_results) >= 10

            WebDriverWait(driver, 5, 0.5).until(check_results_count)
            #modules_found = driver.find_elements_by_xpath('//li[@class="serp-item"]')
            links_found = driver.find_elements_by_xpath('//div[@class="organic__subtitle typo typo_type_greenurl"]')
            #print(len(modules_found), "modules found")
            print(len(links_found), "links found")
            res = 1
            for title in links_found:
                #print(title.text.lower())  # Printing titles found
                if "butylkann.ru" not in title.text.lower():

                    #print("RESULT IS ABSENT ON RESULTS PAGE")
                    pass
                else:
                    print(title.text.lower())
                    print("result present on page, position", res)
                res = res + 1


        print("Test finished OK")

    def tearDown(self):
        self.driver.quit()

if __name__=='__main__':
    unittest.main()
