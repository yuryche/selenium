from selenium.webdriver.chrome.webdriver import WebDriver
from selenium.webdriver.support.wait import WebDriverWait
import time
import unittest
from selenium.webdriver.common.keys import Keys

class Google_test(unittest.TestCase):
    def setUp(self):
        self.driver = WebDriver(executable_path='C://Workspace//chromedriver.exe')
        self.driver.get("https://Google.com")

    def test_01(self):
        driver = self.driver
        input_field = driver.find_element_by_xpath('//input[@class="gLFyf gsfi"]')
        input_field.send_keys("Python")
        input_field.send_keys(Keys.ENTER)
        time.sleep(8)
        titles = driver.find_elements_by_class_name('r')
        for title in titles:
            assert "python" in title.text.lower()

    def test_02(self):
        driver = self.driver
        input_field = driver.find_element_by_xpath('//input[@class="gLFyf gsfi"]')
        input_field.send_keys("Python")
        time.sleep(2)
        input_field.send_keys(Keys.DOWN)
        input_field.send_keys(Keys.DOWN)
        input_field.send_keys(Keys.ENTER)
        time.sleep(8)
        titles = driver.find_elements_by_class_name('r')
        for title in titles:
            assert "*" in title.text.lower()

    def tearDown(self):
        self.driver.quit()

if __name__=='__main__':
    unittest.main()







#driver = WebDriver(executable_path='C://Workspace//chromedriver.exe')
#driver.get("http://www.Google.com")
#assert "Google" in driver.page_source      #check that "Google present in page"
#driver.quit()