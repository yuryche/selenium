from selenium.webdriver.chrome.webdriver import WebDriver
import time
import unittest
from selenium.webdriver.common.keys import Keys

class Mailru_test(unittest.TestCase):
    def setUp(self):
        self.driver = WebDriver(executable_path='C://Workspace//chromedriver.exe')
        self.driver.get("https://mail.ru")
        time.sleep(5)

    def test_01(self):
        driver = self.driver
        news = driver.find_elements_by_xpath('//li[@class="tabs-content__item svelte-10lycdj"]')
        links = driver.find_elements_by_xpath('//li[@class="tabs-content__item svelte-10lycdj"]//a')
        print("In total found", len(news), "and", len(links), "links")
        i = 0
        for x in news:
            print(x.text.lower())
            print(links[i].get_attribute('href'))
            i = i + 1


        print("Test finished OK")

    def tearDown(self):
        self.driver.quit()

if __name__=='__main__':
    unittest.main()
